# Otimiza��o do gradiente conjugado

O presente trabalho, desenvolvido por Adryan Alessandro Signor e Lucas Cicatelli Facchini, sob coordena��o do prof. Ricardo Vargas Dorneles, implementa a otimiza��o do c�lculo do gradiente conjugado utilizando a API do OpenMP, como avalia��o da disciplina de Avalia��o de desempenho.

## Depend�ncias

- OS Linux
- make
- wget
- gcc
- gzip

## Instala��o

No root do projeto execute:

```bash
make
make install
```

## Execu��o

Segue abaixo um exemplo de execu��o do programa:

```bash
./bin/conjugate-gradient hb-files/bcsstk01.rsa
```

Para obter mais informa��es de uso, utilize o comando:

```bash
./bin/conjugate-gradient --usage
```

Para descrever as op��es, utilize o comando:

```bash
./bin/conjugate-gradient --help
```

## Informa��es

Essa se��o descreve informa��es utilizadas no projeto.

### Algoritmo na linguagem MATLAB

Segue abaixo o algoritmo do gradiente conjugado na linguagem MATLAB:

```MATLAB
function x = conjugate_gradient(A, b)
	imax = 1000;
	error = 0.00001;

	n = length(A);
	x = zeros(n, 1);
	i = 1;
	r = b - A * x;
	d = r;

	new_sigma = r' * r;
	sigma0 = new_sigma;

	while (i < imax && new_sigma > error ^ 2 * sigma0)
		q = A * d;

		alpha = new_sigma / (d' * q);
		x = x + alpha * d;

		if (rem(i, 50) == 0)
			r = b - A * x;
		else
			r = r - alpha * q;
		end;

		old_sigma = new_sigma;
		new_sigma = r' * r;

		beta = new_sigma / old_sigma;

		d = r + beta * d;

		i = i + 1;
	end
end
```

### Matrizes

As matrizes est�o armazenadas no reposit�rio [Matrix Market](https://math.nist.gov/MatrixMarket/data/Harwell-Boeing/) em formato texto [Harwell-Boeing Exchange](https://math.nist.gov/MatrixMarket/formats.html#hb).