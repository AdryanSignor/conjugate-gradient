#include <stdio.h>
#include <libgen.h>
#include "../src/cg.h"
#include "../src/hbdata.h"
#include "../src/cgdata.h"

int assert_CG_conjugate_gradient(HB_Matrix *matrix, double *vectorResults, unsigned int numVariables, double *expectedValues) {
	CG_Data *data = CG_conjugate_gradient(matrix, vectorResults, numVariables, CG_MAX_ITERATIONS, CG_EPSILON);

	int equal, result = 1;
	unsigned int i;
	for (i = 0; i < numVariables; i++) {
		equal = expectedValues[i] == data->variables[i];
		result &= equal;

		printf("%c", equal ? '.' : 'F');
	}

	equal = numVariables == data->numVariables;
	result &= equal;

	printf("%c", equal ? '.' : 'F');

	CG_data_free(data);

	return result;
}

int test_CG_conjugate_gradient1() {
	unsigned int pointers[] = {1, 2, 4};
	unsigned int indices[] = {1, 1, 2};
	double values[] = {1, 1, 1};
	HB_Matrix matrix = {3, 3, 3, pointers, indices, values};
	double vectorResults[] = {2, 2};
	double expectedValues[] = {1, 1};

	return assert_CG_conjugate_gradient(&matrix, vectorResults, 2, expectedValues);
}

int test_CG_conjugate_gradient2() {
	unsigned int pointers[] = {1, 2, 3, 4, 5};
	unsigned int indices[] = {1, 2, 3, 4};
	double values[] = {1, 1, 1, 1};
	HB_Matrix matrix = {5, 4, 4, pointers, indices, values};
	double vectorResults[] = {1, 2, 3, 4};
	double expectedValues[] = {1, 2, 3, 4};

	return assert_CG_conjugate_gradient(&matrix, vectorResults, 4, expectedValues);
}

int test_CG_conjugate_gradient_test3() {
	unsigned int pointers[] = {1, 2, 4};
	unsigned int indices[] = {1, 1, 2};
	double values[] = {3, 2, 3};
	HB_Matrix matrix = {3, 3, 3, pointers, indices, values};
	double vectorResults[] = {2, -2};
	double expectedValues[] = {2, -2};

	return assert_CG_conjugate_gradient(&matrix, vectorResults, 2, expectedValues);
}

int main(int const argc, char *argv[]) {
	printf("%-15s ", basename(argv[0]));

	int result = test_CG_conjugate_gradient1()
	& test_CG_conjugate_gradient2()
	& test_CG_conjugate_gradient_test3();

	printf("\n");

	return result ? EXIT_SUCCESS : EXIT_FAILURE;
}
