#include <stdio.h>
#include <libgen.h>
#include "../src/hbfreader.h"
#include "../src/hbdata.h"

int assert_equal_double(double expected, double atual) {
	int result = expected == atual;

	printf("%c", result ? '.' : 'F');

	return result;
}

int assert_HBFR_read(char const *path, double expecSumPointers, double expecSumIndices, double expecSumValues) {
	HB_Matrix *matrix = HBFR_read(path);

	int result = 1;

	double sum = 0;
	unsigned int i;
	for (i = 0; i < matrix->sizePointers; i++) {
		sum += matrix->pointers[i];
	}

	result &= assert_equal_double(expecSumPointers, sum);

	sum = 0;
	for (i = 0; i < matrix->sizeIndices; i++) {
		sum += matrix->indices[i];
	}

	result &= assert_equal_double(expecSumIndices, sum);

	sum = 0;
	for (i = 0; i < matrix->sizeValues; i++) {
		sum += matrix->values[i];
	}

	result &= assert_equal_double(expecSumValues, sum);

	HB_matrix_free(matrix);

	return result;
}

int test_HBFR_read1() {
	return assert_HBFR_read("hb-files/bcsstk01.rsa", 6663, 6538, 39529059817.474434);
}

int test_HBFR_read2() {
	return assert_HBFR_read("hb-files/bcsstk02.rsa", 98088, 98021, 160536.53023181402);
}

int test_HBFR_read3() {
	return assert_HBFR_read("hb-files/bcsstk15.rsa", 124121167, 124825918, 866357199315.0228);
}

int test_HBFR_read4() {
	return assert_HBFR_read("hb-files/bcsstk13.rsa", 40068419, 51259699, 48365468993566.11);
}

int test_HBFR_read5() {
	return assert_HBFR_read("hb-files/bcsstm13.rsa", 8592829, 16433550, 10797.829537276797);
}

int test_HBFR_read6() {
	return assert_HBFR_read("hb-files/bcsstk14.rsa", 30501567, 30332558, 1398994650906.2734);
}

int test_HBFR_read7() {
	return assert_HBFR_read("hb-files/bcsstk21.rsa", 27555111, 27553790, 68695495799.99973);
}

int test_HBFR_read8() {
	return assert_HBFR_read("hb-files/bcsstk25.rsa", 1029386640, 1049619288, 8879509264964554);
}

int test_HBFR_read_no_file() {
	int result = HBFR_read("nofile.txt") == NULL;

	printf("%c", result ? '.' : 'F');

	return result;
}

int main(int const argc, char *argv[]) {
	printf("%-15s ", basename(argv[0]));

	int result = test_HBFR_read1() &
	test_HBFR_read2() &
	test_HBFR_read3() &
	test_HBFR_read4() &
	test_HBFR_read5() &
	test_HBFR_read6() &
	test_HBFR_read7() &
	test_HBFR_read8() &
	test_HBFR_read_no_file();

	printf("\n");

	return result ? EXIT_SUCCESS : EXIT_FAILURE;
}
