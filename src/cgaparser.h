#ifndef CGAPARSER_H
#define CGAPARSER_H

#include <argp.h>
#include <libgen.h>
#include <omp.h>
#include <stdlib.h>
#include "cg.h"

#define CGA_PARALLEL_PROCESSES 'n'

#define CGA_PRINT_VALUES 'p'

#define CGA_MAX_ITERATIONS 'i'

#define CGA_EPSILON 'e'

#define GCA_RUN_FOR_ONE_TO_MAX_THREADS -1

#define GCA_RUN_FOR_MAX_THREADS 0

typedef struct {
	char **files;
	int parallelProcesses;
	unsigned int printValueVariables;
	unsigned int maxIterations;
	double epsilon;
} CGA_Argument;

CGA_Argument *CGA_parse(int const argc, char *argv[]);

error_t CGA_parser(int key, char *arg, struct argp_state *state);

void CGA_parse_parallel_processes(CGA_Argument *argument, unsigned int* ppStart, unsigned int* ppEnd);

void CGA_argument_free(CGA_Argument *argument);

#endif
