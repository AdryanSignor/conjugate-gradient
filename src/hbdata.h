#ifndef HBDATA_H
#define HBDATA_H

#include <stdlib.h>
#include <omp.h>

typedef struct {
	unsigned int sizePointers;
	unsigned int sizeIndices;
	unsigned int sizeValues;
	unsigned int *pointers;
	unsigned int *indices;
	double *values;
} HB_Matrix;

void HB_matrix_product_vector(HB_Matrix *matrix, double *vector, double *result, unsigned int size);

void HB_matrix_free(HB_Matrix *matrix);

#endif
