#include "cgaparser.h"

CGA_Argument *CGA_parse(int const argc, char *argv[]) {
	char descriptionPP[200];
	sprintf(descriptionPP, "Number of parallel processes. Set %d for run with maximum threads (Default). Set %d for run with 1 to maximum threads", GCA_RUN_FOR_MAX_THREADS, GCA_RUN_FOR_ONE_TO_MAX_THREADS);

	struct argp_option options[] = {
		{0, 0, 0, 0, "Process Options:", 7},
		{"parallel-processes", CGA_PARALLEL_PROCESSES, "N", 0, descriptionPP},
		{0, 0, 0, 0, "Conjugate Gradiente Options:", 6},
		{"max-iteration", CGA_MAX_ITERATIONS, "VALUE", 0, "Maximum number of iterations"},
		{"epsilon", CGA_EPSILON, "EPSILON", 0, "Minimal error precision"},
		{0, 0, 0, 0, "Informational Options:", -1},
		{"print-result", CGA_PRINT_VALUES, 0, 0, "Print variables value result"},
		{0}
	};

	struct argp argp = {options, CGA_parser, "[FILE...]", "Program to calcule conjugate gradient"};

	CGA_Argument *argument = malloc(sizeof(CGA_Argument));

	argument->parallelProcesses = GCA_RUN_FOR_MAX_THREADS;
	argument->printValueVariables = 0;
	argument->maxIterations = CG_MAX_ITERATIONS;
	argument->epsilon = CG_EPSILON;

	argp_parse(&argp, argc, argv, 0, 0, argument);

	return argument;
}

error_t CGA_parser(int key, char *arg, struct argp_state *state) {
	CGA_Argument *argument = state->input;

	switch (key) {
		case CGA_PARALLEL_PROCESSES:
			argument->parallelProcesses = atoi(arg);

			if (argument->parallelProcesses < -1 || argument->parallelProcesses > 100000) {
				argp_failure(state, 1, 0, "Number of parallel processes must be >= 1. Set Set %d for run with maximum threads (Default). Set %d for run with 1 to maximum threads", GCA_RUN_FOR_MAX_THREADS, GCA_RUN_FOR_ONE_TO_MAX_THREADS);
			}

			break;

		case CGA_PRINT_VALUES:
			argument->printValueVariables = 1;
			break;

		case CGA_EPSILON:
			argument->epsilon = atof(arg);

			if (argument->epsilon <= 0 || argument->epsilon >= 0.2) {
				argp_failure(state, 1, 0, "Epsilon must be > 0 and < 0.2. Ex.: 0.00001");
			}

			break;

		case CGA_MAX_ITERATIONS:
			argument->maxIterations = atoi(arg);

			break;

		case ARGP_KEY_ARG:
			argument->files = &state->argv[state->next - 1];
			state->next = state->argc;
			break;

		case ARGP_KEY_NO_ARGS:
			argp_usage(state);
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

void CGA_parse_parallel_processes(CGA_Argument *argument, unsigned int* ppStart, unsigned int* ppEnd) {
	omp_set_dynamic(0);
	if (argument->parallelProcesses == GCA_RUN_FOR_MAX_THREADS) {
		*ppStart = *ppEnd = omp_get_num_procs();
	} else if (argument->parallelProcesses == GCA_RUN_FOR_ONE_TO_MAX_THREADS) {
		*ppStart = 1;
		*ppEnd = omp_get_num_procs();
		omp_set_dynamic(1);
	} else {
		*ppStart = *ppEnd = argument->parallelProcesses;
	}
}

void CGA_argument_free(CGA_Argument *argument) {
	if (argument != NULL) {
		free(argument);
	}
}
