#ifndef CGUTILS_H
#define CGUTILS_H

#include <stdlib.h>
#include <stdio.h>
#include <libgen.h>
#include <math.h>

typedef struct {
	char *pathFile;
	unsigned int numThreads;
	unsigned int numIterations;
	double time;
} CGU_Cg_Info;

typedef struct {
	unsigned long hour;
	unsigned long minute;
	unsigned long second;
	unsigned long ms;
} CGU_Time;

void CGU_show_gc_infos(CGU_Cg_Info **infos, unsigned int totalFiles, unsigned int totalThreads);

void CGU_show_cg_info(CGU_Cg_Info *info);

void CGU_print_div(char const *left, char const *middle, char const *right);

void CGU_cg_info_free(CGU_Cg_Info *info);

CGU_Time* CGU_formated_time(double time);

void CGU_time_free(CGU_Time *time);

#endif
