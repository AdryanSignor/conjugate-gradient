#include "cg.h"

void CG_vector_subtraction_vector(double *v1, double *v2, double *result, unsigned int size) {
	unsigned int i;
	for (i = 0; i < size; i++) {
		result[i] = v1[i] - v2[i];
	}
}

double CG_vector_product_vector(double *v1, double *v2, unsigned int size) {
	double sum = 0;
	unsigned int i;
	for (i = 0; i < size; i++) {
		sum += v1[i] * v2[i];
	}

	return sum;
}

void CG_vector_sum_vector(double *v1, double *v2, double *result, unsigned int size) {
	unsigned int i;
	for (i = 0; i < size; i++) {
		result[i] = v1[i] + v2[i];
	}
}

void CG_vector_attribution(double *v1, double *v2, unsigned int size) {
	unsigned int i;
	for (i = 0; i < size; i++) {
		v1[i] = v2[i];
	}
}

void CG_scalar_product_vector(double scalar, double *vector, double *result, unsigned int size) {
	unsigned int i;
	for (i = 0; i < size; i++) {
		result[i] = scalar * vector[i];
	}
}

CG_Data *CG_conjugate_gradient(HB_Matrix *matrixCoefficients, double *vectorResults, unsigned int numVariables, unsigned int maxIterations, double epsilon) {
	CG_Data *data = (CG_Data *) malloc(sizeof(CG_Data));
	data->numIterations = 1;
	data->numVariables = numVariables;
	data->variables = (double *) calloc(numVariables, sizeof(double));

	double *vectorAux = (double *) calloc(numVariables, sizeof(double));
	double *vectorR = (double *) calloc(numVariables, sizeof(double));
	double *vectorD = (double *) calloc(numVariables, sizeof(double));
	double *vectorQ = (double *) calloc(numVariables, sizeof(double));
	double startTime = omp_get_wtime();

	HB_matrix_product_vector(matrixCoefficients, data->variables, vectorAux, numVariables);
	CG_vector_subtraction_vector(vectorResults, vectorAux, vectorR, numVariables);
	CG_vector_attribution(vectorD, vectorR, numVariables);
	double newSigma = CG_vector_product_vector(vectorR, vectorR, numVariables);
	double sigma0 = newSigma;

	while (data->numIterations < maxIterations && newSigma > epsilon * epsilon * sigma0) {
		HB_matrix_product_vector(matrixCoefficients, vectorD, vectorQ, numVariables);

		double alpha = newSigma / CG_vector_product_vector(vectorD, vectorQ, numVariables);
		CG_scalar_product_vector(alpha, vectorD, vectorAux, numVariables);
		CG_vector_sum_vector(data->variables, vectorAux, data->variables, numVariables);

		if (data->numIterations % 50 == 0) {
			HB_matrix_product_vector(matrixCoefficients, data->variables, vectorAux, numVariables);
			CG_vector_subtraction_vector(vectorResults, vectorAux, vectorR, numVariables);
		} else {
			CG_scalar_product_vector(alpha, vectorQ, vectorAux, numVariables);
			CG_vector_subtraction_vector(vectorR, vectorAux, vectorR, numVariables);
		}

		double oldSigma = newSigma;
		newSigma = CG_vector_product_vector(vectorR, vectorR, numVariables);

		double beta = newSigma / oldSigma;

		CG_scalar_product_vector(beta, vectorD, vectorAux, numVariables);
		CG_vector_sum_vector(vectorR, vectorAux, vectorD, numVariables);

		data->numIterations++;
	}

	data->time = omp_get_wtime() - startTime;

	free(vectorAux);
	free(vectorR);
	free(vectorD);
	free(vectorQ);

	return data;
}
