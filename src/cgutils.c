#include "cgutils.h"

void CGU_show_gc_infos(CGU_Cg_Info **infos, unsigned int totalFiles, unsigned int totalThreads) {
	unsigned int i, j, infoIndex;
	for (i = 0; i < totalFiles; i++) {
	CGU_print_div("╔", "╦", "╗");
		printf("║ %-20s ║ Threads ║ %13s ║ %-17s ║ %-18s ║\n", "File", "Iterations", "Time in seconds", "Time");
		CGU_print_div("╠", "╬", "╣");
		for (j = 0; j < totalThreads; j++) {
			infoIndex = i + j * totalFiles;
			CGU_show_cg_info(infos[infoIndex]);
			CGU_cg_info_free(infos[infoIndex]);
		}

		CGU_print_div("╚", "╩", "╝");
	}
}

void CGU_show_cg_info(CGU_Cg_Info *info) {
	CGU_Time* time = CGU_formated_time(info->time);

	printf(
		"║ %-20s ║ %7d ║ %13d ║ %017.6lf ║ %02ldh %02ld\' %02ld\'\' %03ldms ║\n",
		basename(info->pathFile),
		info->numThreads,
		info->numIterations,
		info->time,
		time->hour,
		time->minute,
		time->second,
		time->ms
	);

	CGU_time_free(time);
}

void CGU_print_div(char const *left, char const *middle, char const *right) {
	printf(
		"%s══════════════════════%s═════════%s═══════════════%s═══════════════════%s════════════════════%s\n",
		left,
		middle,
		middle,
		middle,
		middle,
		right
	);
}

void CGU_cg_info_free(CGU_Cg_Info *info) {
	if (info != NULL) {
		free(info);
	}
}

CGU_Time* CGU_formated_time(double time) {
	CGU_Time *cguTime = malloc(sizeof(CGU_Time));
	cguTime->ms = round((time - (long)time) * 1000);
	cguTime->second = (unsigned long) time % 60;
	cguTime->minute = (unsigned long) time / 60 % 60;
	cguTime->hour = (unsigned long) time / 3600 % 3600;

	return cguTime;
}

void CGU_time_free(CGU_Time *time) {
	if (time != NULL) {
		free(time);
	}
}
