#include "cgdata.h"

void CG_data_free(CG_Data *data) {
	if (data != NULL) {
		if (data->variables != NULL) {
			free(data->variables);
		}

		free(data);
	}
}
