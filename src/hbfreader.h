#ifndef HBFREADER_H
#define HBFREADER_H

#include "hbdata.h"

#define HBFR_LINE_WIDTH 81

typedef struct {
	unsigned int totcrd;
	unsigned int ptrcrd;
	unsigned int indcrd;
	unsigned int valcrd;
	unsigned int rhscrd;
	unsigned int nrow;
	unsigned int ncol;
	unsigned int nnzero;
	unsigned int neltvl;
	unsigned int ptrfmt;
	unsigned int indfmt;
	unsigned int valfmt;
} HBFR_Header;

typedef struct {
	unsigned int totalDataElements;
	unsigned int totalDataRowElements;
	unsigned int elementTypeSize;
	char *elementFormat;
} HBFR_Block_Info;

HB_Matrix *HBFR_read(char const *path);

void HBFR_read_header(FILE *file, HBFR_Header *header);

void *HBFR_read_block(FILE *file, HBFR_Block_Info *info);

void HBFR_read_pointers(FILE *file, HBFR_Header *header, HB_Matrix *matrix);

void HBFR_read_indices(FILE *file, HBFR_Header *header, HB_Matrix *matrix);

void HBFR_read_numerical_values(FILE *file, HBFR_Header *header, HB_Matrix *matrix);

void HBFR_ignore_spaces_no_data_end_line(FILE *file);

#endif
