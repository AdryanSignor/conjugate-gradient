#ifndef CG_H
#define CG_H

#include "cgdata.h"
#include "hbdata.h"

#define CG_MAX_ITERATIONS 10000000

#define CG_EPSILON 0.00001

CG_Data *CG_conjugate_gradient(HB_Matrix *matrixCoefficients, double *vectorResults, unsigned int numVariables, unsigned int maxIterations, double epsilon);

void CG_vector_subtraction_vector(double *v1, double *v2, double *result, unsigned int size);

double CG_vector_product_vector(double *v1, double *v2, unsigned int size);

void CG_vector_sum_vector(double *v1, double *v2, double *result, unsigned int size);

void CG_vector_attribution(double *v1, double *v2, unsigned int size);

void CG_scalar_product_vector(double scalar, double *vector, double *result, unsigned int size);

#endif
