#ifndef CGDATA_H
#define CGDATA_H

#include <stdlib.h>

typedef struct {
	unsigned int numIterations;
	double time;
	unsigned int numVariables;
	double *variables;
} CG_Data;

void CG_data_free(CG_Data *data);

#endif
