#include "hbdata.h"

void HB_matrix_product_vector(HB_Matrix *matrix, double *vector, double *result, unsigned int size) {
	int numThreads = omp_get_max_threads();
	double *aux = (double *) calloc(numThreads * size, sizeof(double));
	unsigned int i;

	#pragma omp parallel for
	for (i = 0; i < size; i++) {
		unsigned int auxStartIndex = omp_get_thread_num() * size;
		unsigned int startRange = matrix->pointers[i] -1;
		unsigned int endRange = matrix->pointers[i + 1] -1;
		unsigned int j;

		for (j = startRange; j < endRange; j++) {
			unsigned int index = matrix->indices[j] - 1;
			double value = matrix->values[j];

			aux[auxStartIndex + index] += vector[i] * value;
			if (index != i) {
				aux[auxStartIndex + i] += vector[index] * value;
			}
		}
	}

	#pragma omp parallel for
	for (i = 0; i < size; i++)	{
		result[i] = 0;
		unsigned int j;
		for (j = 0; j < numThreads; j++) {
			result[i] += aux[j * size + i];
		}
	}

	free(aux);
}

void HB_matrix_free(HB_Matrix *matrix) {
	if (matrix != NULL) {
		if (matrix->pointers != NULL) {
			free(matrix->pointers);
		}

		if (matrix->indices != NULL) {
			free(matrix->indices);
		}

		if (matrix->values != NULL) {
			free(matrix->values);
		}

		free(matrix);
	}
}
