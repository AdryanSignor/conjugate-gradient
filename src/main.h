#ifndef CG_MAIN
#define CG_MAIN

#include "cg.h"
#include "cgdata.h"
#include "cgaparser.h"
#include "hbfreader.h"
#include "hbdata.h"
#include "cgutils.h"

int run(CGA_Argument *argument, unsigned int totalFiles, CGU_Cg_Info **infos);

int main(int const argc, char *argv[]);

#endif
