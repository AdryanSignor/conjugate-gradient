#include "main.h"

int main(int const argc, char *argv[]) {
	int result = EXIT_SUCCESS;
	CGA_Argument *argument = CGA_parse(argc, argv);

	unsigned int i, totalFiles = 0;
	for (i = 0; argument->files[i]; i++) {
		totalFiles++;
	}

	unsigned int ppStart, ppEnd;
	CGA_parse_parallel_processes(argument, &ppStart, &ppEnd);

	unsigned int totalThreads = ppEnd - ppStart + 1;
	CGU_Cg_Info **infos = malloc(sizeof(CGU_Cg_Info *) * totalFiles * totalThreads);

	for (i = ppStart; i <= ppEnd; i++) {
		omp_set_num_threads(i);
		if (run(argument, totalFiles, infos)) {
			result = EXIT_FAILURE;
			break;
		}
	}

	if (result == EXIT_SUCCESS && !argument->printValueVariables) {
		CGU_show_gc_infos(infos, totalFiles, totalThreads);
	}

	free(infos);
	CGA_argument_free(argument);

	return result;
}

int run(CGA_Argument *argument, unsigned int totalFiles, CGU_Cg_Info **infos) {
	unsigned int numThreads = omp_get_max_threads();
	unsigned int fileIndex;
	for (fileIndex = 0; argument->files[fileIndex]; fileIndex++) {
		HB_Matrix *matrix = HBFR_read(argument->files[fileIndex]);

		if (matrix == NULL) {
			printf("Can't open file %s\n", argument->files[fileIndex]);

			return 1;
		}

		double *vectorResults = (double *) malloc((matrix->sizePointers - 1) * sizeof(double));

		unsigned int i;
		for (i = 0; i < matrix->sizePointers - 1; i++) {
			vectorResults[i] = 1;
		}

		CG_Data *data = CG_conjugate_gradient(matrix, vectorResults, matrix->sizePointers - 1, argument->maxIterations, argument->epsilon);

		if (argument->printValueVariables) {
			printf("%s\n", basename(argument->files[fileIndex]));
			printf("Threads: %d\n", numThreads);
			for (i = 0; i < matrix->sizePointers - 1; i++) {
				printf("%+.10lf\n", data->variables[i]);
			}
			printf("\n");
		} else {
			unsigned int indexInfo = (omp_get_dynamic() ? (numThreads - 1) : 0) * totalFiles + fileIndex;
			infos[indexInfo] = malloc(sizeof(CGU_Cg_Info));
			infos[indexInfo]->pathFile = argument->files[fileIndex];
			infos[indexInfo]->numThreads = numThreads;
			infos[indexInfo]->numIterations = data->numIterations;
			infos[indexInfo]->time = data->time;
		}

		HB_matrix_free(matrix);
		free(vectorResults);
		CG_data_free(data);
	}

	return 0;
}
