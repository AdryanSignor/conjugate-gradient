BIN_DIR = bin/
SRC_DIR = src/
TESTS_DIR = tests/
MKDIR_BIN = mkdir -p $(BIN_DIR)

HB_FILES_DIR = hb-files/
FTP_HB_FILES_DIR = ftp://math.nist.gov/pub/MatrixMarket2/Harwell-Boeing/

CC = gcc
CFLAGS = -Wall -fopenmp -lm
OBJS = $(patsubst %.c, %.o, $(wildcard $(SRC_DIR)*.c))
TESTS = $(wildcard $(TESTS_DIR)*.c)
MAIN_NAME = $(BIN_DIR)conjugate-gradient

prepare:
ifeq ($(wildcard $(HB_FILES_DIR)*),)
	$(MAKE) download-hb-files
endif
	$(MAKE) test

install:
	$(MKDIR_BIN)
	$(MAKE) compile-main
	$(MAKE) clean-objects

test:
ifeq ($(wildcard $(HB_FILES_DIR)*),)
	$(MAKE) download-hb-files
endif
	$(MKDIR_BIN)
	$(MAKE) compile-tests
	$(MAKE) run-tests
	$(MAKE) clean-objects
	$(MAKE) clean-tests

download-hb-files:
	rm -rf $(HB_FILES_DIR)
	mkdir $(HB_FILES_DIR)
	wget -P $(HB_FILES_DIR) -r -nd -A '*.rsa.gz' $(FTP_HB_FILES_DIR)bcsstruc1 $(FTP_HB_FILES_DIR)bcsstruc2 $(FTP_HB_FILES_DIR)bcsstruc3
	gunzip $(HB_FILES_DIR)*

compile-main: $(SRC_DIR)main.o $(OBJS)
	$(CC) $(CFLAGS) -o $(MAIN_NAME) $(patsubst $(SRC_DIR)%, $(BIN_DIR)%, $?)

compile-tests: $(filter-out $(SRC_DIR)main.o, $(OBJS))
	$(foreach TEST, $(TESTS), $(CC) $(CFLAGS) -o $(BIN_DIR)$(basename $(notdir $(TEST))) $(TEST) $(patsubst $(SRC_DIR)%, $(BIN_DIR)%, $?);)

run-tests:
	$(foreach TEST, $(TESTS), ./$(BIN_DIR)$(basename $(notdir $(TEST)));)

clean-objects:
	rm $(BIN_DIR)*.o -f

clean-tests:
	rm $(BIN_DIR)*_test -f

%.o: %.c
	$(CC) $(CFLAGS) -c -o $(BIN_DIR)$(notdir $@) $<
